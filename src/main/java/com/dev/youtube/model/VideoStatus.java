package com.dev.youtube.model;

public enum VideoStatus {
    PUBLIC,
    PRIVATE,
    UNLISTED
}
