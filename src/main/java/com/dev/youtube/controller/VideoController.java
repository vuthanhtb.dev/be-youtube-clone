package com.dev.youtube.controller;

import com.dev.youtube.dto.CommentDto;
import com.dev.youtube.dto.UploadVideoRepository;
import com.dev.youtube.dto.VideoDto;
import com.dev.youtube.service.VideoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/videos")
@RequiredArgsConstructor
public class VideoController {

    private final VideoService videoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UploadVideoRepository uploadVideo(@RequestParam("file") MultipartFile file) {
        return this.videoService.uploadVideo(file);
    }

    @PostMapping("/thumbnail")
    @ResponseStatus(HttpStatus.CREATED)
    public String uploadThumbnail(@RequestParam("file") MultipartFile file, @RequestParam("id") String id) {
        return this.videoService.uploadThumbnail(file, id);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public VideoDto editVideoMetadata(@RequestBody VideoDto videoDto) {
        return this.videoService.editVideo(videoDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public VideoDto getVideoDetail(@PathVariable String id) {
        return this.videoService.getVideoDetail(id);
    }

    @PostMapping("/{id}/like")
    @ResponseStatus(HttpStatus.OK)
    public VideoDto likeVideo(@PathVariable String id) {
        return this.videoService.likeVideo(id);
    }

    @PostMapping("/{id}/disLike")
    @ResponseStatus(HttpStatus.OK)
    public VideoDto disLikeVideo(@PathVariable String id) {
        return this.videoService.disLikeVideo(id);
    }

    @PostMapping("/{videoId}/add-comment")
    @ResponseStatus(HttpStatus.OK)
    public void addComment(@PathVariable String videoId, @RequestBody CommentDto commentDto) {
        this.videoService.addComment(videoId, commentDto);
    }

    @GetMapping("/{videoId}/comment")
    @ResponseStatus(HttpStatus.OK)
    public List<CommentDto> getAllComments(@PathVariable String videoId) {
        return this.videoService.getAllComments(videoId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<VideoDto> getAllVideos() {
        return this.videoService.getAllVideos();
    }
}
