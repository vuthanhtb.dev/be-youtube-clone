package com.dev.youtube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeYoutubeCloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeYoutubeCloneApplication.class, args);
    }

}
