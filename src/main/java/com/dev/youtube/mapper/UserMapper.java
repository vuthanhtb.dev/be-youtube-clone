package com.dev.youtube.mapper;

import com.dev.youtube.dto.UserInfoDTO;
import com.dev.youtube.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    public abstract User toUser(UserInfoDTO userInfoDTO);

    public abstract UserInfoDTO toUserInfoDTO(User user);
}
