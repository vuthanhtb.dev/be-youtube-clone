package com.dev.youtube.mapper;

import com.dev.youtube.dto.CommentDto;
import com.dev.youtube.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(componentModel = "spring")
public abstract class CommentMapper {

    @Mapping(target = "text", source = "commentText")
    public abstract Comment toComment(CommentDto commentDto);

    @Mapping(target = "commentText", source = "text")
    public abstract CommentDto toCommentDto(Comment comment);

    public List<CommentDto> toListCommentDto(Stream<Comment> comments) {
        if (comments == null) {
            return null;
        }
        return comments.map(comment -> toCommentDto(comment))
                .collect(Collectors.toCollection(ArrayList<CommentDto>::new));
    }
}
