package com.dev.youtube.mapper;

import com.dev.youtube.dto.VideoDto;
import com.dev.youtube.model.Video;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(componentModel = "spring")
public abstract class VideoMapper {

    @Mapping(target = "viewCount", expression = "java(getViewCount(video))")
    @Mapping(target = "likes", expression = "java(getLike(video))")
    @Mapping(target = "disLikes", expression = "java(getDisLike(video))")
    public abstract VideoDto toVideoDto(Video video);

    Integer getViewCount(Video video) {
        return video.getViewCount().get();
    }

    Integer getLike(Video video) {
        return video.getLikes().get();
    }

    Integer getDisLike(Video video) {
        return video.getDisLikes().get();
    }

    public List<VideoDto> toListVideoDto(Stream<Video> videos) {
        if (videos == null) {
            return null;
        }
        return videos.map(video -> toVideoDto(video))
                .collect(Collectors.toCollection(ArrayList<VideoDto>::new));
    }
}
