package com.dev.youtube.service;

import com.dev.youtube.model.User;
import com.dev.youtube.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public User getCurrentUser() {
        String sub = ((Jwt) (SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getClaim("sub");

        return this.userRepository
                .findBySub(sub)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Cannot find user with sub - %s", sub)));
    }

    public void addToLikedVideos(String id) {
        User currentUser = getCurrentUser();
        currentUser.addToLikeVideos(id);
        this.userRepository.save(currentUser);
    }

    public boolean ifLikedVideo(String id) {
        return this.getCurrentUser()
                .getLikedVideos()
                .stream()
                .anyMatch(likedVideo -> likedVideo.equals(id));
    }

    public boolean ifDisLikedVideo(String id) {
        return this.getCurrentUser()
                .getDisLikedVideos()
                .stream()
                .anyMatch(likedVideo -> likedVideo.equals(id));
    }

    public void removeFromLikedVideos(String id) {
        User currentUser = getCurrentUser();
        currentUser.removeFromLikedVideos(id);
        this.userRepository.save(currentUser);
    }

    public void removeFromDislikedVideos(String id) {
        User currentUser = getCurrentUser();
        currentUser.removeFromDislikedVideos(id);
        this.userRepository.save(currentUser);
    }

    public void addToDisLikedVideos(String id) {
        User currentUser = getCurrentUser();
        currentUser.addToDislikedVideos(id);
        this.userRepository.save(currentUser);
    }

    public void addVideoToHistory(String id) {
        User currentUser = getCurrentUser();
        currentUser.addToVideoHistory(id);
        this.userRepository.save(currentUser);
    }

    public void subscribeUser(String id) {
        User currentUser = getCurrentUser();
        currentUser.addToSubscribedToUsers(id);

        User user = getUserById(id);
        user.addToSubscribers(currentUser.getId());

        this.userRepository.save(currentUser);
        this.userRepository.save(user);
    }

    public void unSubscribeUser(String id) {
        User currentUser = getCurrentUser();
        currentUser.removeFromSubscribedToUsers(id);

        User user = getUserById(id);
        user.removeFromSubscribers(currentUser.getId());

        this.userRepository.save(currentUser);
        this.userRepository.save(user);
    }

    public Set<String> userHistory(String id) {
        User user = getUserById(id);
        return user.getVideoHistory();
    }

    private User getUserById(String id) {
        return this.userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Cannot find user with userId %s", id)));
    }
}
