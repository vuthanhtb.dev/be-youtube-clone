package com.dev.youtube.service;

import com.dev.youtube.dto.CommentDto;
import com.dev.youtube.dto.UploadVideoRepository;
import com.dev.youtube.dto.VideoDto;
import com.dev.youtube.mapper.CommentMapper;
import com.dev.youtube.mapper.VideoMapper;
import com.dev.youtube.model.Comment;
import com.dev.youtube.model.Video;
import com.dev.youtube.repository.VideoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequiredArgsConstructor
public class VideoService {
    private final S3Service s3Service;
    private final VideoRepository videoRepository;
    private final UserService userService;
    private final VideoMapper videoMapper;
    private final CommentMapper commentMapper;

    public UploadVideoRepository uploadVideo(MultipartFile file) {
        String videoUrl = this.s3Service.uploadFile(file);
        Video video = new Video();
        video.setVideoUrl(videoUrl);

        Video savedVideo = this.videoRepository.save(video);
        return new UploadVideoRepository(savedVideo.getId(), savedVideo.getVideoUrl());
    }

    public VideoDto editVideo(VideoDto videoDto) {
        Video video = this.getVideoById(videoDto.getId());

        video.setTitle(videoDto.getTitle());
        video.setDescription(videoDto.getDescription());
        video.setTags(videoDto.getTags());
        video.setVideoStatus(videoDto.getVideoStatus());
        video.setThumbnailUrl(videoDto.getThumbnailUrl());

        this.videoRepository.save(video);
        return videoDto;
    }

    public String uploadThumbnail(MultipartFile file, String id) {
        Video video = this.getVideoById(id);
        String thumbnailUrl = this.s3Service.uploadFile(file);

        video.setThumbnailUrl(thumbnailUrl);
        this.videoRepository.save(video);
        return thumbnailUrl;
    }

    private Video getVideoById(String id) {
        return this.videoRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException(String.format("cannot find video by id: %s", id)));
    }

    public VideoDto getVideoDetail(String id) {
        Video video = getVideoById(id);

        return this.videoMapper.toVideoDto(video);
    }

    public VideoDto likeVideo(String id) {
        Video video = getVideoById(id);

        if (this.userService.ifLikedVideo(id)) {
            video.decrementLikes();
            this.userService.removeFromLikedVideos(id);
        } else if (this.userService.ifDisLikedVideo(id)) {
            video.decrementDisLikes();
            this.userService.removeFromDislikedVideos(id);
            video.incrementLikes();
            this.userService.addToLikedVideos(id);
        } else {
            video.incrementLikes();
            this.userService.addToLikedVideos(id);
        }

        this.videoRepository.save(video);
        return this.videoMapper.toVideoDto(video);
    }

    public VideoDto disLikeVideo(String id) {
        Video video = getVideoById(id);

        if (this.userService.ifDisLikedVideo(id)) {
            video.decrementDisLikes();
            this.userService.removeFromDislikedVideos(id);
        } else if (this.userService.ifLikedVideo(id)) {
            video.decrementLikes();
            this.userService.removeFromLikedVideos(id);
            video.incrementDisLikes();
            this.userService.addToDisLikedVideos(id);
        } else {
            video.incrementDisLikes();
            this.userService.addToDisLikedVideos(id);
        }

        this.videoRepository.save(video);

        return this.videoMapper.toVideoDto(video);
    }

    public void addComment(String videoId, CommentDto commentDto) {
        Video video = getVideoById(videoId);
        Comment comment = this.commentMapper.toComment(commentDto);
        video.addComment(comment);

        this.videoRepository.save(video);
    }

    public List<CommentDto> getAllComments(String videoId) {
        Video video = getVideoById(videoId);
        List<Comment> commentList = video.getCommentList();

        return this.commentMapper.toListCommentDto(commentList.stream());
    }

    public List<VideoDto> getAllVideos() {
        var streamVideo = this.videoRepository.findAll().stream();
        return this.videoMapper.toListVideoDto(streamVideo);
    }
}
